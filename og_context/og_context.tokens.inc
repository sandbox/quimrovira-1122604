<?php

/**
 * @file
 * Builds placeholder replacement tokens for Organic groups context data.
 */

/**
 * Implements hook_token_info().
 */
function og_context_token_info() {
  $types['current-group'] = array(
    'name' => t('Current group'),
    'description' => t('Tokens related to the current group as found via OG\'s context module settings.'),
    'type' => 'group',
  );

  return array(
    'types' => $types,
  );
}

/**
 * Implements hook_tokens().
 */
function og_context_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();

  if ($type == 'current-group') {
    if ($group = og_context()) {
      $replacements += token_generate('group', $tokens, array('group' => $group), $options);
    }
  }

  return $replacements;
}

