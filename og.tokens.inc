<?php

/**
 * @file
 * Builds placeholder replacement tokens for Organic groups related data.
 */

/**
 * Implements hook_token_info().
 */
function og_token_info() {
  $types['group'] = array(
    'name' => t('Organic groups'),
    'description' => t('Tokens related to Organic groups.'),
    'needs-data' => 'group',
  );

  $group['gid'] = array(
    'name' => t('Group ID'),
    'description' => t("The unique ID of the group."),
  );

  $group['label'] = array(
    'name' => t('Label'),
    'description' => t("The label (i.e. name) of the group."),
  );

  $group['url'] = array(
    'name' => t("URL"),
    'description' => t("The URL of the group."),
  );

  $group['created'] = array(
    'name' => t("Created"),
    'description' => t("The date the user account was created."),
    'type' => 'date',
  );

  $group['node'] = array(
    'name' => t('Main group node'),
    'description' => t('The main node that identifies this group.'),
    'type' => 'node',
  );

  $group['manager'] = array(
    'name' => t('Manager user'),
    'description' => t('Tokens related to the group manager user.'),
    'type' => 'user',
  );

  return array(
    'types' => $types,
    'tokens' => array('group' => $group),
  );
}

/**
 * Implements hook_tokens().
 */
function og_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $url_options = array('absolute' => TRUE);
  if (isset($options['language'])) {
    $url_options['language'] = $options['language'];
    $language_code = $options['language']->language;
  }
  else {
    $language_code = NULL;
  }
  $sanitize = !empty($options['sanitize']);

  $replacements = array();
  if ($type == 'group' && !empty($data['group'])) {
    $group = $data['group'];
    foreach ($tokens as $name => $original) {
      switch ($name) {
        // Basic user account information.
        case 'gid':
          if(!empty($group->gid)) {
            $replacements[$original] = $group->gid;
          }
          break;

        case 'label':
          $replacements[$original] = og_label($group->gid, $sanitize);
          break;

        case 'node':
        case 'url':
          if (($entity = $group->getEntity()) && ($uri = entity_uri($group->entity_type, $entity)) && !empty($uri['path'])) {
            $replacements[$original] = url($uri['path'], $url_options);
          }
          break;

        case 'created':
          // In the case of user_presave the created date may not yet be set.
          if (!empty($group->created)) {
            $replacements[$original] = format_date($group->created, 'medium', '', NULL, $language_code);
          }
          break;

        case 'manager':
            if(($entity = $group->getEntity()) && ($account = user_load($entity->uid))) {
              $replacements[$original] = $sanitize ? check_plain($account->name) : $account->name;
            }
          break;

      }
    }

    if ($registered_tokens = token_find_with_prefix($tokens, 'created')) {
      $replacements += token_generate('date', $registered_tokens, array('date' => $group->created), $options);
    }

    if ($node_tokens = token_find_with_prefix($tokens, 'manager')) {
      if (($entity = $group->getEntity()) && ($account = user_load($entity->uid))) {
        $replacements += token_generate('user', $node_tokens, array('user' => $account), $options);
      }
    }

    if ($group->entity_type == "node") {
      if ($node_tokens = token_find_with_prefix($tokens, 'node')) {
        if ($entity = $group->getEntity()) {
          $replacements += token_generate('node', $node_tokens, array('node' => $entity), $options);
        }
      }
    }

  }

  return $replacements;
}
